<?php

namespace Tests;

use CurrencyConverter\BINHandler;
use GuzzleHttp\Client;
use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Psr7\Response;
use PHPUnit\Framework\TestCase;

class BINHandlerTest extends TestCase
{
    public function testHandle()
    {
        $handler = $this->getHandler();
        $handler->handle('USD');

        self::assertObjectHasAttribute('bin', $handler);
    }

    public function testIsEU()
    {
        $handler = $this->getHandler();
        $handler->handle('USD');

        self::assertTrue($handler->isEU());
    }

    /**
     * @return BINHandler
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \JsonException
     */
    private function getHandler(): BINHandler
    {
        $_ENV['BIN_PROVIDER_URL'] = '/';
        $_ENV['EU_MEBER_STATES'] = 'AT,BE,BG,CY,CZ,DE,DK,EE,ES,FI,FR,GR,HR,HU,IE,IT,LT,LU,LV,MT,NL,PO,PT,RO,SE,SI,SK';

        $mock = new MockHandler(
            [
                new Response(
                    200,
                    [],
                    '{"number":{},"scheme":"mastercard","type":"debit","brand":"Debit","country":{"numeric":"440","alpha2":"LT","name":"Lithuania","emoji":"🇱🇹","currency":"EUR","latitude":56,"longitude":24},"bank":{}}'
                ),
            ]
        );

        $handlerStack = HandlerStack::create($mock);
        $client = new Client(['handler' => $handlerStack]);

        return new BINHandler($client);
    }
}
