<?php

namespace Tests;

use CurrencyConverter\Converter;
use PHPUnit\Framework\TestCase;

class ConverterTest extends TestCase
{

    public function testProcess()
    {
        $mock = $this->createMock(Converter::class);
        $mock->expects(self::once())
            ->method('process')
            ->willReturnCallback(
                function () {
                    $data = [1, 0.41, 1.58, 2.14, 43.34];
                    foreach ($data as $e) {
                        yield $e;
                    }
                }
            );

        $mock->process();
    }
}
