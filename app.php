<?php

use CurrencyConverter\BINHandler;
use CurrencyConverter\Converter;
use CurrencyConverter\ExchangeRatesHandler;
use GuzzleHttp\Client;

require __DIR__ . '/vendor/autoload.php';

$dotenv = Dotenv\Dotenv::createImmutable(__DIR__);
$dotenv->load();

$dotenv->required(
    [
        'BASE_CURRENCY',
        'EU_MEBER_STATES',
        'BIN_PROVIDER_URL',
        'RATES_PROVIDER_URL'
    ]
)->notEmpty();
$path = $argv[1];

try {
    $binHandler = new BINHandler();
    $ratesHandler = new ExchangeRatesHandler();

    $converter = new Converter($path, $binHandler, $ratesHandler);
    $conversionGenerator = $converter->process();

    foreach ($conversionGenerator as $item) {
        echo $item . PHP_EOL;
    }
} catch (Throwable $e) {
    echo "Cannot process exchange rates";
}
