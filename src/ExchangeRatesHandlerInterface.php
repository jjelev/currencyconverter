<?php

namespace CurrencyConverter;

interface ExchangeRatesHandlerInterface
{
    /**
     * @param string $currency
     * @param float $amount
     * @param float $correction
     * @return float
     */
    public function handle(string $currency, float $amount, float $correction): float;
}