<?php

namespace CurrencyConverter;

use GuzzleHttp\Client;

class BINHandler implements BINHandlerInterface
{
    private \stdClass $bin;
    /**
     * @var string[]
     */
    private array $EUMembers;

    /**
     * BINHandler constructor.
     */
    public function __construct()
    {
        $this->EUMembers = explode(',', strtoupper($_ENV['EU_MEBER_STATES']));
    }

    /**
     * @param $iin string
     * @return $this
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \JsonException
     */
    public function handle(string $iin): BINHandlerInterface
    {
        $client = new Client();
        $response = $client->get($_ENV['BIN_PROVIDER_URL'] . '/' . $iin);

        $this->bin = json_decode($response->getBody(), false, 512, JSON_THROW_ON_ERROR);

        return $this;
    }

    /**
     * @return bool
     */
    public function isEU(): bool
    {
        // This is ok for the scope of this project. JSON validation must be used otherwise.
        $symbol = strtoupper($this->bin->country->alpha2 ?? '');

        return in_array($symbol, $this->EUMembers, true);
    }
}