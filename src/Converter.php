<?php

namespace CurrencyConverter;

use Generator;

class Converter
{
    private string $filename;
    private BINHandlerInterface $binHandler;
    private ExchangeRatesHandlerInterface $ratesHandler;

    /**
     * Converter constructor.
     * @param string $filename
     * @param BINHandlerInterface $binHandler
     * @param ExchangeRatesHandlerInterface $ratesHandler
     */
    public function __construct(
        string $filename,
        BINHandlerInterface $binHandler,
        ExchangeRatesHandlerInterface $ratesHandler
    ) {
        $this->filename = $filename;
        $this->binHandler = $binHandler;
        $this->ratesHandler = $ratesHandler;
    }

    /**
     * @return Generator
     * @throws \JsonException
     */
    public function process(): Generator
    {
        $handle = fopen($this->filename, 'rb');

        while (!feof($handle)) {
            $line = fgets($handle);

            $dataRow = json_decode($line, false, 512, JSON_THROW_ON_ERROR);

            $correction = $this->binHandler->handle($dataRow->bin)->isEU() ? 0.01 : 0.02;

            yield $this->ratesHandler->handle($dataRow->currency, (float)$dataRow->amount, $correction);
        }
    }
}