<?php

namespace CurrencyConverter;

interface BINHandlerInterface
{
    /**
     * @param string $iin
     * @return BINHandlerInterface
     */
    public function handle(string $iin): BINHandlerInterface;

    /**
     * @return bool
     */
    public function isEU(): bool;
}