<?php

namespace CurrencyConverter;

use GuzzleHttp\Client;

class ExchangeRatesHandler implements ExchangeRatesHandlerInterface
{
    private string $baseCurrency;
    private array $rates;

    /**
     * ExchangeRatesHandler constructor.
     * @param Client|null $client
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \JsonException
     */
    public function __construct()
    {
        $this->baseCurrency = strtoupper($_ENV['BASE_CURRENCY']);

        $this->rates ??= $this->getRates();
    }

    /**
     * @param string $currency
     * @param float $amount
     * @param float $correction
     * @return float
     */
    public function handle(string $currency, float $amount, float $correction): float
    {
        $currency = strtoupper($currency);

        /*
         * It is safe to say it preserves the original logic
         * Nullable situations are handled by dividing to 1 which is not possible if API data is correct
         * However I presume from original that this behavior is logically correct.
         */
        $rate = $this->rates["rates"][$currency] ?? 1;

        $value = ($currency === $this->baseCurrency ? $amount : $amount / $rate) * $correction;

        $pow = 10 ** 2;
        return ceil($value * $pow) / $pow;
    }

    /**
     * @return array
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \JsonException
     */
    private function getRates(): array
    {
        $client = new Client();

        $response = $client->get(
            $_ENV['RATES_PROVIDER_URL'],
            [
                'query' => ['base' => $this->baseCurrency]
            ]
        );

        return json_decode($response->getBody(), true, 512, JSON_THROW_ON_ERROR);
    }
}